import { CategoryInterface } from "./category.interface";

export interface CatInterface {
    label: string;
    id: string;
    url: string;
    breeds: any;
    width: number,
    height: number,
    categories: Array<CategoryInterface>
  }