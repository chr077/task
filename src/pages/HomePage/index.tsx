import { useEffect, useState, useCallback } from 'react';
import { categoryActions, catActions } from '../../redux/actions';
import { useAppDispatch, useAppSelector } from '../../redux';
import SideBar from '../../components/Sidebar';
import List from '../../components/List';

import { Container, ContainerSpinner, Title, Button } from './styled';
import Spinner from '../../components/Spinner';

const HomePage = () => {
    const dispatch = useAppDispatch()

    const categories = useAppSelector(state => state.categoryData.categories);
    const categoryResponse = useAppSelector(state => state.categoryData.categoriesGotResponse);
    const catsGotResponse = useAppSelector(state => state.catData.catsGotResponse);
    const cats = useAppSelector(state => state.catData.cats);

    const [page, setPage] = useState(1);
    const [category, setCategory] = useState('');


  useEffect(() => {
    dispatch(categoryActions.getAllCategories())
  }, [dispatch]);

  useEffect(() => {
    dispatch(catActions.getCats('10', '1'))
  }, [dispatch]);

  const handleCategory = useCallback((id: string) => {
    dispatch(catActions.getCats('10','1', id))
    setCategory(id);
  }, [dispatch]);

  const handleMore = useCallback(() => {
   dispatch(catActions.getMoreCats('10', page.toString(), category))
   setPage(page+1)
  }, [category, dispatch, page] )

  return (
    <Container>
      <Title> Cat Images</Title>
      {categoryResponse && categories.length > 0 && <SideBar data={categories}  onClickCategory={handleCategory}/>}
      {catsGotResponse && <List data={cats} />}
      {!categoryResponse && !catsGotResponse && <ContainerSpinner><Spinner /></ContainerSpinner>}
      <Button onClick={handleMore}>Load More</Button>
    </Container>
  );
}
export default HomePage;