
import styled from 'styled-components';

export const Container = styled.div`
  display: flex;
  flex-direction: row;
  width: 100%;
  height: 100%;
`;


export const ContainerSpinner = styled.div`
  position: fixed;
  left: 50%;
  top: 50%; 
`;

export const Title = styled.h1`
    position: fixed;
    left: 50%;
`;

export const Button = styled.button`
  position: fixed;
  right: 50px;
  bottom: 10px; 
  width: 150px;
  height: 70px;
  z-index: 30;
  background-color: #312450;
  background-image: linear-gradient(180deg, rgba(255, 255, 255, .15), rgba(255, 255, 255, 0));
  border: 1px solid #312450;
  border-radius: 1rem;
  box-shadow: rgba(255, 255, 255, 0.15) 0 1px 0 inset,rgba(46, 54, 80, 0.075) 0 1px 1px;
  box-sizing: border-box;
  color: #FFFFFF;
  cursor: pointer;
  letter-spacing: 0.2em;
  color: white;

  &:hover {
    background-color: #1d152f;;
  }
`;
