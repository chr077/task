import { BrowserRouter, Routes, Route, Navigate } from 'react-router-dom';
import  store  from './redux';
import { Provider } from 'react-redux';
import HomePage from './pages/HomePage';

function App() {

  return (
    <Provider store={store}>
      <BrowserRouter>
        <Routes>
          <Route
              path={'/home'}
              element={<HomePage />}
           />
          <Route
            path="*"
            element={<Navigate to="/home" replace />}
          />
        </Routes>
      </BrowserRouter>
    </Provider>
  );
}

export default App;  