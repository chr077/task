
import styled from 'styled-components';

export const Li = styled.li`
  position: relative;
  padding: 3.2em 0;
  list-style-type: none;
`;


export const Item = styled.div`
  line-height: 2em;
  text-transform: uppercase;
  text-decoration: none;
  letter-spacing: 0.2em;
  color: white;
  display: block;
  transition: all ease-out 300ms;
  cursor: pointer;
  text-align: center;
  font-size: larger;
`;