import { Li, Item } from './styled';

interface SidebarItemType {
  id: string;
  name: string;
  onClickCategory?: (id: string) => void;
}

const SidebarItem = ({ id, name, onClickCategory }: SidebarItemType) => {


  const handleClick = () => {
    onClickCategory && onClickCategory(id);
  }
  return (
    <Li><Item onClick={handleClick}>{name}</Item></Li>
  );
}
export default SidebarItem;