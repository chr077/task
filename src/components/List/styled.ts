
import styled from 'styled-components';

export const Image = styled.img`
  width: 300px;
`;

export const Container = styled.div`
  position: fixed;
  left: 25%;
  display: flex;
  width: 75%;
  height:100%;
  flex-direction: row;
  flex-wrap: wrap;
  margin: 120px 16px;
  overflow: scroll;
`;

export const Item = styled.div`
  margin: 8px;
`;