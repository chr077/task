import { useMemo } from 'react';
import { Image, Item, Container } from './styled';
import { CatInterface } from '../../interfaces/cat.interface';


interface SidebarType {
  data?: Array<CatInterface>;
}

const Sidebar = ({ data }: SidebarType) => {

 const items = useMemo( () => data && data.map((item: CatInterface) => {return <Item><Image src={item.url} loading="lazy" key={item.id}/></Item>}), [data]);

  return (
      <Container>
        {items}
      </Container>
  );
}
export default Sidebar;