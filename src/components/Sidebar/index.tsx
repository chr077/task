import { useMemo } from 'react';
import { SidebarStyled, Container } from './styled';
import SidebarItem from '../SidebarItem';
import { CategoryInterface } from '../../interfaces/category.interface';


interface SidebarType {
  data?: Array<CategoryInterface>;
  onClickCategory?: (id: string) => void;
}

const List = ({ data, onClickCategory }: SidebarType) => {

 const items = useMemo( () => data && data.map((item: CategoryInterface) => {return <SidebarItem id={item.id} name={item.name} key={item.id} onClickCategory={onClickCategory}/>}), [data]);

  return (
    <SidebarStyled>
      <Container>
        {items}
      </Container>
    </SidebarStyled>

  );
}
export default List;