
import styled from 'styled-components';

export const SidebarStyled = styled.div`
  position: fixed;
  width: 20%;
  height: 100%;
  background: #312450;
  font-size: 0.65em;
`;


export const Container = styled.div`
  display: flex;
  flex-direction: column;
  padding: 100px 0;
`;