import { fetchCats } from '../../data/cat.data';
import { catConstants } from '../constants';
  

  export const getCats = (limit: string, page: string, category?: string) => async (dispatch: (arg0: { type: string; payload: any }) => any) => {
    try {
      const data = await fetchCats(limit, page, category);
      dispatch({ type: catConstants.FETCH_CATS, payload: data });
    } catch (e) {
      console.error(e);
    }
  };
  

  export const getMoreCats = (limit: string, page: string, category?: string) => async (dispatch: (arg0: { type: string; payload: any }) => any) => {
    try {
      const data = await fetchCats(limit, page, category);
      dispatch({ type: catConstants.LOAD_MORE, payload: data });
    } catch (e) {
      console.error(e);
    }
  };


  // eslint-disable-next-line import/no-anonymous-default-export
  export default { getCats, getMoreCats };
  