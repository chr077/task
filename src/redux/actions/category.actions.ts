import {
    fetchAllCategories
   } from '../../data/category.data';
   import { categoryConstants } from '../constants';
   
   const getAllCategories = () => async (dispatch: (arg0: { type: string; payload: any }) => any) => {
     try {
       const data = await fetchAllCategories();
       dispatch({ type: categoryConstants.FETCH_CATEGORIES, payload: data });
     } catch (e) {
       console.error(e);
     }
   };
   
 
   export default {
    getAllCategories
   };
   