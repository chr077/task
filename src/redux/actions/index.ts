import catActions from './cat.actions';
import categoryActions from './category.actions';

export {
    catActions, categoryActions,
};