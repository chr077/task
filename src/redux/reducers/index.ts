import categoryData from './categoryData.reducer';
import catData from './catData.reducer';

export {
  categoryData,
  catData,
};