import { categoryConstants } from '../constants';

const initialState = {
  categories: [],
  categoriesGotResponse: false,
};
function categoryData(state = initialState, action: { type: string; payload?: any }) {
  switch (action.type) {
    case categoryConstants.FETCH_CATEGORIES:
      return {
        categories: action.payload,
        categoriesGotResponse: true,
      };

    default:
      return state;
  }
}

export default categoryData;
