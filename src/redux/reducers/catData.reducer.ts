import { catConstants } from '../constants';

const initialState = {
  cats: [],
  catsGotResponse: false,
};
function catData(state = initialState, action: { type: string; payload?: any }) {
  switch (action.type) {
    case catConstants.FETCH_CATS:
      return {
        cats: action.payload,
        catsGotResponse: true,
      };
    case catConstants.LOAD_MORE:
      return {
        cats: state.cats ? [...state.cats, ...action.payload] : action.payload,
        catsGotResponse: true,
      };

    default:
      return state;
  }
}

export default catData;
