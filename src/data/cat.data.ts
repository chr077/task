import fetchWithHeaders, { apiHost } from './helper';


  /**
 * method: GET,
 * response: [
 *    {
 *        id: string,
          url: string,
          breeds: array,
          width: number,
          height: number,
          categories?:[{id: number,name: string}]
 *    }
 * ]
 */


  export async function fetchCats(limit: string, page: string, category?: string) {
    const fetchUrl =  category ? `${apiHost}/images/search?limit=${limit}&page=${page}&category_ids=${category}`  : 
        `${apiHost}/images/search?limit=${limit}&page=${page}`;

    const response = await fetchWithHeaders(fetchUrl)
      .then((res) => res.json())
      .catch((err) => {
        // eslint-disable-next-line no-console
        console.log('Unable to get cats', err);
      });
        
    return response;
}