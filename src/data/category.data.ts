import fetchWithHeaders, { apiHost } from './helper';

/**
 * method: GET,
 * response: [
 *    {
 *        id: number,
          type: string,
 *    }
 * ]
 */

   export async function fetchAllCategories() {
      const fetchUrl = `${apiHost}/categories`;
      const response = await fetchWithHeaders(fetchUrl)
        .then((res) => res.json())
        .catch((err) => {
          // eslint-disable-next-line no-console
          console.log('Unable to get categories', err);
        });
          
      return response;
  }