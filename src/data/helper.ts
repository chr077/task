const fetchWithHeaders = async (fetchUrl: string, options?: any) => {
    const optionsWithAuth = {
      ...options,
    };
    return fetch(fetchUrl, optionsWithAuth);
  };
  

  export default fetchWithHeaders;
  export const apiHost = 'https://api.thecatapi.com/v1';
